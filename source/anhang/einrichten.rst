.. meta::

   :description lang=de: Virtuelle Umgebung einrichten für Sphinx.
   :keywords: Sphinx, Virtual Environment, RevealJS

=====================
Ersteinrichtung/Setup
=====================

Präsentation: `Einrichtung und Nutzung einer virtuellen Umgebung <../_static/presi/index.html>`_ für Windows-Nutzer.

Für den Einsteiger sind folgende Schritte zu prüfen
und gegebenfalls durchzuführen (zur Orientierung):

.. image:: ../_static/sphinx-install.svg
   :width: 50%
