:orphan:

.. _glossar:
   
=========
 Glossar
=========

.. glossary::
   
   Direktiven (directives)

      
      Steuern und Konfigurieren das verhalten und aussehen von Objekten, z.B.

      - Bilder
      - Scripte
      - Todo-Einträge

      Sie beginnen immer mit zwei Punkten, gefolgt von einem Leerzeichen,
      dem Namen der Direktive und schließen mit zwei Doppelunkten ab. Je
      nach Objekt-Typ folgen weitere Parameter.

      .. code:: 

	 .. image:: meinBild.webp
	    :alt: Das bin ich.
  
   
   Rollen (roles)

      Steuerkommandos, eingeschlossen in ein Doppelpunktpaar (:ref:)
      gefolgt von Parmetern mit einem  Akzentzeichen am Anfag bzw. Ende
      (`Beschriftung <Sprungmarke>`).   

      .. code:: 

	 :ref:`Meine Linkliste <meineLinkliste>`
