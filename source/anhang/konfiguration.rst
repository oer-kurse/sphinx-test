.. meta::

   :description lang=de: Sphinx -- Python: Konfiguration
   :keywords: Sphinx, Konfiguration

=============
Konfiguration
=============

Die Datei *conf.py* sammelt alle Standardeinstellungen, die angepasst
und ergänzt werden können.

.. literalinclude:: ./files/conf.py
   :language: python
