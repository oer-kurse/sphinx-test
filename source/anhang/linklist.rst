:orphan:
   
.. _linkliste:

..index:: Linkliste

============
 Linksliste
============

Zum Thema Dokumentation:

- Docutils als Basis für Sphinx
  
  https://docutils.sourceforge.io 

- Arten der Dokumentation
  
  https://documentation.divio.com 

- Schwerpunkt Mathematik, IPython
  
  https://matplotlib.org/sampledoc/index.html

- Syntax-Hervorhebung -- Liste unterstützter Sprachen
  
  https://pygments.org/docs/lexers/

- Warum Dokuentieren, der erste Schritt sein sollte.
  
  https://www.linuxjournal.com/content/three-ways-improve-your-programming-skills
  
- Ausführbare Dokumentation

  https://jupyterbook.org/intro.html
