==================
 Wie alles begann
==================

Rückblick [#f1]_

- 1996

  Zope (Jim Fulton)

- 2002

  PEP 287 [#f2]_
    
Später dann als Docutils, ein Teil des Python-Universum. [#f3]_

.. [#f1] https://docutils.sourceforge.io/docs/ref/rst/introduction.html#goals
.. [#f2] https://www.python.org/dev/peps/pep-0287/
.. [#f3] https://docutils.sourceforge.io/ 
