=======================
 Grundlegende Konzepte
=======================
.. index:: Konzepte

Im Mittelpunkt jeder Dokumentation steht das erfassen von Gedanken,
Ideen, Erkenntissen. Für die Strukturierung des Wissens bedient man
sich einer Auszeichnungssprache. Sphinx bietet dafür eine Punktnotation,
in der zwei Konzepte bessonders oft vorkommen:

.. _20240118T103856:

- Direktiven
- Rollen

.. index:: Direktiven
  
Direktiven
==========

.. image:: ./images/direktive.png
   :width: 300px

**Beispiel**

Das Einbinden einer Bilddatei:

::

   .. image:: ./images/direktive.png
      :width: 300px
   
.. index:: Rollen
	   
Rollen
======

Es handelt sich um semantisches Markup, das von der
Standard-Auszeichnung (Default)  abweichend, das Verhalten ändert.

.. image:: ./images/role.png

**Beispiel: Tastenkürzel**
	   
Aus normalen Buchstaben wird ein Tastendruckbeispiel:

::
   
   :kbd:`C-x`, :kbd:`C-f`  

:kbd:`C-x`, :kbd:`C-f`  

**Beispiel: selbstdefinierte Farben**

.. raw:: html

   <style>

   .rot {
      color: red;
    }

   .blau {
      color: blue;
   }
   </style>

Der Name der Rolle (»role« )wird als css-Klasse gespeichert und das gewünschte
css muss vorher lokal oder zentral definiert werden.
   
:: 

   .. role:: rot
   .. role:: blau

   Das wird  :rot:`rot gefärbt`, gefolgt von :blau:`blau gefärbt`.

.. role:: rot
	  
.. role:: blau

Das wird  :rot:`rot gefärbt`, gefolgt von :blau:`blau gefärbt`.

Das daszugehörige CSS
+++++++++++++++++++++

Lokal als raw-Block am Anfang der gleichen Datei. 

.. code:: css

   .. raw:: html

      <style>

      .rot {
         color: red;
      }

      .blau {
         color: blue;
      }
      </style>

   
Fehlerquellen
=============

.. note:: Nicht korrekt eingerückte Textpassagen!

   - Prüfen Sie bitte immer erst die Einrückung, für die verwendete Direktive. 
   - Negative Einrückung hebt die Wirkung der vorherigen Direktive wieder auf.
