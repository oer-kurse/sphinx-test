========
Git-logs
========

.. index:: Erweiterung: git-log

.. |a| image:: hi-souls.svg

.. sidebar:: Serie: Altägypten

   | 
   | |a|
   |
   | Die Seelen der Götter


Quelle:

https://sphinx-git.readthedocs.io/en/latest/getting-started.html#including-sphinx-git-in-your-project

Installation
=============

::
   
   pip install sphinx-git

Konfiguration
=============

In der Datei conf.py:

::

   extensions = [
    'sphinx_git',
   ]


   
Logeinträge einbinden
=====================

::

   .. git_changelog::


.. hint:: Funkioniert auf GitLab nicht, deshalb hier nur ein Schnappschuss.


So sieht es dann aus:
---------------------

.. raw:: html

   
	 <ul class="simple">
	   <li><p><strong>sphinx-git aktiviert</strong><span> by </span><em>PeterK</em><span> at </span><em>2022-03-08 12:43:16</em></p></li>
	   <li><p><strong>Test Extensions</strong><span> by </span><em>PeterK</em><span> at </span><em>2022-03-08 10:49:36</em></p></li>
	   <li><p><strong>Installation auf requirements umgestellt</strong><span> by </span><em>PeterK</em><span> at </span><em>2022-03-07 15:16:07</em></p></li >
 	   <li><p><strong>Extensions integriert</strong><span> by </span><em>PeterK</em><span> at </span><em>2022-03-05 09:25:54</em></p></li>
	   <li><p><strong>Theming-Themen eingebunden</strong><span> by </span><em>PeterK</em><span> at </span><em>2022-03-04 10:58:55</em></p></li>
	   <li><p><strong>Sprachen – Einstiegsordner</strong><span> by </span><em>PeterK</em><span> at </span><em>2022-02-25 12:56:32</em></p></li>
	   <li><p><strong>Formeln und Pfad zu tables korrigiert</strong><span> by </span><em>PeterK</em><span> at </span><em>2022-02-25 12:48:13</em></p></li>
	   <li><p><strong>Glossary</strong><span> by </span><em>PeterK</em><span> at </span><em>2022-02-25 12:44:03</em></p></li>
	   <li><p><strong>Anhang mit besonderen Dateien</strong><span> by </span><em>PeterK</em><span> at </span><em>2022-02-25 12:41:13</em></p></li>
	   <li><p><strong>Themen zum Inhalte erstellen</strong><span> by </span><em>PeterK</em><span> at </span><em>2022-02-25 12:29:12</em></p></li>
	 </ul>
