==========
 Graphviz
==========

.. index:: Graphviz

.. |a| image:: hi-battlemented.svg

.. sidebar:: Serie: Altägypten

   | 
   | |a|
   |
   | Umfriedung mit Zinnen



- `Dokumentation zu Graphviz <https://www.sphinx-doc.org/en/master/usage/extensions/graphviz.html>`_

.. hint:: Graphviz-Tools müssen installiert und verfügbar sein.


Konfiguration
-------------

.. code:: Python
	  
   extensions = [
       'sphinx.ext.graphviz',
   ]
   graphviz_output_format = 'svg'


Beispiel 1: Graphviz-Workflow
-----------------------------

Quellcode
+++++++++
Beispiel übernommen von Jürgen Hermann [#f1]_.

.. code::

   .. graphviz::
       :name: sphinx.ext.graphviz
       :caption: Sphinx and GraphViz Data Flow [#fn1]_
       :alt: How Sphinx and GraphViz Render the Final Document
       :align: center

	digraph "sphinx-ext-graphviz" {
	    size="6,4";
	    rankdir="LR";
	    graph [fontname="Verdana", fontsize="12"];
	    node [fontname="Verdana", fontsize="12"];
	    edge [fontname="Sans", fontsize="9"];

	    sphinx [label="Sphinx", shape="component",
		      href="https://www.sphinx-doc.org/",
		      target="_blank"];
	    dot [label="GraphViz", shape="component",
		 href="https://www.graphviz.org/",
		 target="_blank"];
	    docs [label="Docs (.rst)", shape="folder",
		  fillcolor=green, style=filled];
	    svg_file [label="SVG Image", shape="note", fontcolor=white,
		    fillcolor="#3333ff", style=filled];
	    html_files [label="HTML Files", shape="folder",
		 fillcolor=yellow, style=filled];

	    docs -> sphinx [label=" parse "];
	    sphinx -> dot [label=" call ", style=dashed, arrowhead=none];
	    dot -> svg_file [label=" draw "];
	    sphinx -> html_files [label=" render "];
	    svg_file -> html_files [style=dashed];
	}

Ergebnis
++++++++

.. image:: ./images/graphviz-workflow.svg
   :width: 600px

Beispiel 2: ein Hassediagramm
-----------------------------

Quellcode
+++++++++

In externer Datei:

.. code::

   graph  {
     a c d e h 
     d -- a;
     e -- c;
     e -- d;
     h -- e;
   }

Einbinden
+++++++++

::
   
   .. graphviz:: ./hassediagramm.dot

Ergebnis
++++++++

.. image:: ./images/hassediagram.svg
   :width: 300px

     
Fußnoten
++++++++

.. [#f1] https://jhermann.github.io/blog/python/documentation/2020/03/25/sphinx_ext_graphviz.html

