==========
 Plantuml
==========

.. meta::

   :description lang=de: Sphinx Erweiterung »plantuml« für grafische Darstellungen 
   :keywords: Sphinx, Plantuml, Grafik

.. index:: UML (Plantuml)

.. |a| image:: hi-man.svg

.. sidebar:: Serie: Altägypten

   | 
   | |a|
   |
   | Mann schaut über die Schulter.


- Prototypen für Benutzeroberflächen: https://plantuml.com/salt

Installation
============

.. code:: bash

   pip install sphinxcontrib-plantuml

Konfiguration
=============

.. code:: Python

   extensions = ['sphinxcontrib.plantuml']

   # plantuml = 'java -jar ../utils/plantum.jar'


Grafik erstellen in Plain Text
==============================

.. code::

   .. uml:: 


   @startuml
   actor "Sphinx Anwender" as ShxAnw #aliceblue;line:blue;line.bold;text:blue;
   
   ShxAnw -> ( verwende \nSphinx und PlantUML)

   note left of ShxAnw
   Hallo zu Linuxtag!   
   end note
   
   @enduml


.. image:: ./images/uml-linuxtage.svg
   :width: 400px
   
.. uml

   @startuml
   actor "Sphinx Anwender" as ShxAnw #aliceblue;line:blue;line.bold;text:blue;
   
   ShxAnw -> ( verwende \nSphinx und PlantUML)

   note left of ShxAnw
   Hallo zu Linuxtag!   
   end note
   
   @enduml

