Org2Sphinx
==========

Ich schreibe viel im EMACS, kombiniert mit dem Org-Mode. Was liegt
näher, als ein fehlendes Bindeglied selber zu entwickeln. Es ist nicht
perfekt, optimiert den Workflow beginnend mit einer org-Datei, über eine
rst-Datei, hin zur publikationsreifen html-Datei. Das Projekt ist noch jung
und befindet sich im Beta-Status. 

Schematische Darstellung
------------------------


.. image:: ./images/org2sphinx.svg


Erweiterung
-----------

Programm und Dokumentation sind zu finden unter:

https://gitlab.com/opendata-apps/org2sphinx

Alternativen
------------

Nikola ist ein Projekt, das ebenfalls auf Sphinx basiert und über ein
Plugin org-Dateien verarbeiten kann.

- `Nikola <https://getnikola.com/blog>`_
- `Plugin für org-Dateien in Nikiola <https://plugins.getnikola.com/v8/orgmode/>`_
