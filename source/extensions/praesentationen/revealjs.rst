.. _folien:

======================
 Folien mit reveal.js
======================

.. index:: Erweiterung: revealjs

.. |a| image:: hi-authority.svg

.. sidebar:: Serie: Altägypten

   | 
   | |a|
   |
   | Mann mit Zepter (Autorität)

   
RevealJS ist eine bekannte JavaSript-Lösung für Foliensätze,
die sich im Browser und damit Systemunabhängig präsentieren
lassen.

Ein Beispiel und die nötigen Schritte sind in der folgenden
Präsentation beschrieben:

`Folien mit Installationsanleitung für Sphinx + »reveal.js«
<https://inkubator.sudile.com/kurse/sphinx-revealjs/index.html>`_
