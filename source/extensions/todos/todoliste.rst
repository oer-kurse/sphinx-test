===========
ToDo-Listen
===========

.. index:: Erweiterung: git-log

.. |a| image:: hi-authority.svg

.. sidebar:: Serie: Altägypten

   | 
   | |a|
   |
   | Mann mit Zepter (Autorität)

   
mit Todo-Einträgen kann man auf unvollendete Texte verweisen.

Extension einbinden in die conf.py
::

   extensions = [
    'sphinx.ext.todo',
   ]

   todo_include_todos = True

Ein Eintrag sieht wie folgt aus:
::

   .. todo:: Was noch zu tun ist....

Die Liste der Todo-Einträge inclusive eines Links zur fraglichen
Stelle kann dann wie folgt eingebunden werden.

::

   .. todolist::

      
.. raw:: html

   <section id="ergebnis" class="active">
   <h2>Ergebnis<a class="headerlink" href="#ergebnis" title="Link zu dieser Überschrift">¶</a></h2>
   <div class="admonition-todo admonition">
   <p class="admonition-title">Zu tun</p>
   <p>Mehr zum Thema Theming</p>
   </div>
	 <p class="todo-source">(Der <a class="reference internal"
	 href="../../templating/eigenes-css.html#id1">
	 <em>ursprüngliche Eintrag</em></a> steht in
	 stations/templating/eigenes-css.rst, Zeile 28.)</p>
   </section>

