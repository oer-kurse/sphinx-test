.. meta::

   :description lang=de: Sphinx -- Python: Gloasary
   :keywords: gloassary, Sphinx, Python


==========
 Glossary
==========

.. index:: Glossar

.. |a| image:: ./images/hi-scrible.svg

.. sidebar:: Serie: Altägypten

   |a|
   
   Schreibutensilien


Definitionen, Begriffserklärungen...
====================================


Quelle:
-------
::

  .. glossary::

     cn 
        Common Name

     ou 
        Organizational Unit

     dc 
        Domain Component

Ergebnis
--------

.. glossary::

   cn 
      Common Name

   ou 
      Organizational Unit

   dc 
      Domain Component  


      
