===================
 Inhalte erstellen
===================

.. image:: ./images/hi-man-with-oar.svg
   :width: 0px
    
.. index:: Inhalte erstellen

.. |a| image:: ../../_images/hi-man-with-oar.svg

.. sidebar:: Serie: Altägypten
   
   |a|
   
   Mann mit Ruder

.. toctree::
   :maxdepth: 1
   :caption: Einleitung
   :glob:

   bilder/*
   fussnoten/*
   tables/*
   */*
-  :ref:`Farben (Rollen-Konzept) <20240118T103856>`

