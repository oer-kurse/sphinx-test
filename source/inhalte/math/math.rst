=======
Formeln
=======

.. |a| image:: ./images/hi-eine-millionen.svg

.. sidebar:: Serie: Altägypten

   |a|
   
   Eine Millionen

Formel I
========

.. code::
  
   .. math::

      (a + b)^2 = a^2 + 2ab + b^2

      (a - b)^2 = a^2 - 2ab + b^2

Formel II
=========
      
.. code::

  .. math::
     :nowrap:

     \begin{eqnarray}
        y    & = & ax^2 + bx + c \\
        f(x) & = & x^2 + 2xy + y^2
     \end{eqnarray}

Formel III
==========

.. code::
  
  .. math:: e^{i\pi} + 1 = 0
     :label: euler

  Euler's identity, equation :eq:`euler`, was elected one of the most
  beautiful mathematical formulas.


Ausgabe für Formel I
====================
      
.. math::

   (a + b)^2 = a^2 + 2ab + b^2

   (a - b)^2 = a^2 - 2ab + b^2

Ausgabe für Formel II
=====================


.. math::
   :nowrap:

   \begin{eqnarray}
      y    & = & ax^2 + bx + c \\
      f(x) & = & x^2 + 2xy + y^2
   \end{eqnarray}

   
Ausgabe für Formel III
======================


.. math:: e^{i\pi} + 1 = 0
   :label: euler

Euler's identity, equation :eq:`euler`, was elected one of the most
beautiful mathematical formulas.
