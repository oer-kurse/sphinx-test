.. index:: Scripte einbinden

===================
 Skripte einbinden
===================


.. index:: Quellcode
.. index:: Skripte

.. |a| image:: ./images/hi-write-engrave.svg

.. sidebar:: Serie: Altägypten
   
   |a|
   
   Schreiben/eingravieren 


Code und Skripte können includiert  oder direkt eingebunden werden.
 
Komplett
--------

Quelle
::

  .. literalinclude:: files/screenrc
     :language: bash


Ergebnis

.. literalinclude:: files/screenrc
   :language: bash


Ausgewählte Zeilen
------------------

Quelle
::

  .. literalinclude:: files/screenrc
     :language: bash
     :lines: 1, 4-5

Ergebnis

.. literalinclude:: files/screenrc
   :language: bash
   :lines: 1, 4-5

Zeilennummern einblenden
------------------------

Quelle
::

  .. literalinclude:: files/screenrc
     :language: bash
     :lines: 1, 4-5
     :linenos:

Ergebnis

.. literalinclude:: files/screenrc
   :language: bash
   :lines: 1, 4-5
   :linenos:



Zeilennummern einblenden
------------------------

Quelle
::

  .. literalinclude:: files/screenrc
     :language: bash
     :linenos:
     :emphasize-lines: 3,5

Ergebnis

.. literalinclude:: files/screenrc
   :language: bash
   :linenos:
   :emphasize-lines: 3,5

Codeblöcke
----------
Statt eine Datei einzubinden, kann der Code auch direkt in den Text integriert werden.

Quelle

::

  .. code-block:: python
     :emphasize-lines: 3,5

     def some_function():
         interesting = False
         print('This line is highlighted.')
         print('This one is not...')
         print('...but this one is.')

Ergebnis

.. code-block:: python
   :emphasize-lines: 3,5

   def some_function():
       interesting = False
       print('This line is highlighted.')
       print('This one is not...')
       print('...but this one is.')
