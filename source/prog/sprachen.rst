====================
Sprach-Unterstützung
====================

Eine sicher unvollständige Liste:

- Python
- C
- C++
- Javascript
- reStructuredText


Syntax-Hervorhebung
===================

Für über 100 Sprachen (:ref:`siehe auch Linkliste <linkliste>`)
  
Beispiele
=========

Mit Sphinx realisierte Projekte:

- `ein auf Sphinx spezialisiertes Online-Angebot <https://readthedocs.org>`_
  
Das wohl mit Abstand umfangreichste Projekt »Read the Docs«! Nach eigener Aussage (Stand vom Januar/2022):

  - 55 million Seiten/Monat ausgeliefert
  - 40 TB Speicherplatz für Documentationen/Monat
  - 80000 Open Source Projekte von 100000 Benutzern
  - und das alles, verwaltet durch ein kleines Team

- `Python Dokumentation <https://docs.python.org/3/>`_
- `Typo 3 Dokumentation <https://docs.typo3.org/m/typo3/docs-how-to-document/main/en-us/Index.html>`_
  
  

