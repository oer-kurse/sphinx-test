==================
 Komplettlösungen
==================
.. index:: Software; Bloggen
.. index:: Bloggen; Software
.. meta::

   :description lang=de: Bloggen mit Python-Software: Nikola und ABlog
   :keywords: Bloggen, Nikoal, ABlog 

.. image:: ./images/hi-giraffen.svg
   :width: 0

.. |a| image:: /_images/hi-giraffen.svg

.. sidebar:: Serie: Altägypten

   |a|

   Mann auf zwei Giraffen


Wenn in dieser Dokumentation die Standard-Sphinx-Syntax im Mittelpunkt
steht, kann mit diversen Komplettlösungen, ein erweiterter
Funktionsumfang genutzt werden.

Nachfolgend eine Liste:

- `Nikola (rst, org, md) <https://getnikola.com/blog/>`_
- `ABlog (rst) <https://ablog.readthedocs.io/en/latest/index.html>`_
- `Pelican (rst, md) <https://getpelican.com/>`_
  
