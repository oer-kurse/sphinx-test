=========
Metadaten
=========


.. INDEX:: Metadaten; Keywords
.. INDEX:: Keywords; Metadaten
.. meta::

   :description lang=de: Sphinx bietet viele Möglichkeiten Wissen zu ordnen und zu publizieren.
   :keywords: Sphinx, Publikation, Metadaten, Suchmaschinen

.. image:: ./images/hi-gefaess.svg
   :width: 0

.. |a| image:: ./images/hi-gefaess.svg

.. sidebar:: Serie: Altägypten

   |a|

   Gefäß



Für die Suchmaschinen sind die Metadaten wichtig. Damit sind die
Meta-Angaben zum Inhalt wie auch die Schlüsselbegriffe wichtig.
Es gibt sicher auch andere Parameter, nach denen die
Ranking-Algorithmen die Seite bewerten, aber es gehört zum guten
Inhalt dazu.

Wie kann das umgesetzt werden?


------------


::

   .. meta::

      :description lang=de: Sphinx bietet viele Möglichkeiten,
                            Wissen zu ordnen und zu publizieren.
      :keywords: Sphinx, Publikation, Metadaten, Suchmaschinen

Wie sieht es im Quelltext aus?

.. code:: html

   <head><meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width,initial-scale=1"/>
    <meta name="color-scheme" content="light dark">
    <meta content="Sphinx bietet viele Möglichkeiten Wissen zu ordnen
    und zu publizieren." lang="de" name="description" xml:lang="de" />
   <meta content="Sphinx, Publikation, Metadaten, Suchmaschinen" name="keywords" />
