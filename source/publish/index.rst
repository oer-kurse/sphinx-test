.. index:: Publizieren

	   
===========
Publizieren
===========

.. |a| image:: ./images/hi-hand-with-egg.svg

.. sidebar:: Serie: Altägypten

   |a|
   
   Hand, ein Ei haltend

:ref:`Folien <folien>`

.. toctree::
   :maxdepth: 1

   bloggen/index
   export/loesungen
   export/gitlab
   export/docset
   ../../extensions/praesentationen/revealjs

